import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CartModule} from "./cart/cart.module";
import {ProductsModule} from "./products/products.module";
import {ProductComponent} from "./products/product/product.component";



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CartModule,
    ProductsModule
  ],
})
export class FeaturesModule { }
